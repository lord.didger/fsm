#include "policy.hpp"

template<>
fsm::all_actions<machine> fsm::transition(::machine& m, state::A& s, event::alpha)
{
    std::cout << "A" << std::endl;
    if(s.flag)
    {
        return fsm::action::change<state::B>();
    }
    else
    {
        return fsm::action::change<state::C>();
    }
}

template<>
fsm::all_actions<machine> fsm::transition(::machine&, state::B&)
{
    std::cout << "B" << std::endl;
    return fsm::action::change<state::C>();
}

template<>
fsm::all_actions<machine> fsm::transition(::machine&, state::C&)
{
    std::cout << "C" << std::endl;
    return fsm::action::change<state::A>();
}
