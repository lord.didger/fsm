#pragma once
#include "fsm/transition.hpp"
#include "machine.hpp"

template<>
fsm::all_actions<machine> fsm::transition(::machine& m, state::A& s, event::alpha);

template<>
fsm::all_actions<machine> fsm::transition(::machine&, state::B&);

template<>
fsm::all_actions<machine> fsm::transition(::machine&, state::C&);
