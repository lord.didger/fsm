#pragma once
#include "fsm/machine.hpp"

namespace state
{
    struct A
    {
        bool flag;
    };
    struct B {};
    struct C {};
}

namespace event
{
    struct alpha {};
}

using machine = fsm::machine<fsm::states<state::A,state::B,state::C>, fsm::storage::no_memory>;
