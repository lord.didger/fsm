#include <random>
#include "fsm/process.hpp"
#include "machine.hpp"

int main()
{
    // initialize state machine
    std::random_device rd;
    machine m;
    fsm::storage::store(m.storage, state::A { .flag = static_cast<bool>(rd() % 2) });

    // use state machine
    process(m, event::alpha());
    process(m);
    process(m);
}
