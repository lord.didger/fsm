#pragma once
#include <iostream>
#include "action.hpp"
#include "utils/wrap.hpp"

namespace fsm
{
    template<typename M>
    using changed_actions = wrap_t<action::change, typename M::states::set>;

    template<typename M>
    using all_actions = sum_t<sum_t<action::none>, changed_actions<M>>;

    template<typename M, typename S, typename ...E>
    all_actions<M> transition(M& m, S& s, E... e)
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
        return action::none{};
    }
}
