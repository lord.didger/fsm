#pragma once
#include "storage/no_memory.hpp"
#include "storage/with_memory.hpp"
#include "utils/apply.hpp"

namespace fsm
{
    template<typename ...S>
    struct states
    {
        using set = std::variant<S...>;
    };

    template<typename States, template<typename> typename Storage>
    struct machine
    {
        using states = States;
        using storage_t = apply_t<Storage, typename states::set>;
        storage_t storage;
    };
}
