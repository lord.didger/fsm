#pragma once
#include <variant>

namespace impl
{
    template<template<typename> typename T, typename R>
    struct apply
    {
        using type = T<R>;
    };

    template<template<typename> typename T, typename ...Ts>
    struct apply<T, std::variant<Ts...>>
    {
        using type = T<Ts...>;
    };
}

template<template<typename> typename T, typename R>
using apply_t = typename impl::apply<T,R>::type;
