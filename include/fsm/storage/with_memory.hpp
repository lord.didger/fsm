#pragma once
#include <tuple>
#include <variant>
#include "../utils/wrap.hpp"

namespace fsm::storage
{
    template<typename ...S>
    struct with_memory;

    namespace impl
    {
        template<typename T>
        struct marker
        {
            using type = T;
        };

        template<typename S, typename ...T>
        void mark(with_memory<T...>& m)
        {
            m.m = marker<S>();
        }
    }

    template<typename ...S>
    struct with_memory
    {
        wrap_t<impl::marker, std::variant<S...>> m;
        std::tuple<S...> t;
    };

    template<typename S, typename ...T>
    S& get(with_memory<T...>& m)
    {
        return std::get<S>(m.t);
    }

    template<typename S, typename ...T>
    void store(with_memory<T...>& m, S&& s)
    {
        impl::mark<S>(m);
        get<S>(m) = std::forward<S>(s);
    }

    template<typename S, typename ...T>
    void store(with_memory<T...>& m)
    {
        impl::mark<S>(m);
    }

    template<typename F, typename ...T>
    auto visit(F&& f, with_memory<T...>& m)
    {
        return std::visit(
            [&](auto marker)
            {
                return f(get<typename decltype(marker)::type>(m));
            },
            m.m
        );
    }
}
