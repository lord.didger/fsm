#pragma once
#include <variant>

namespace fsm::storage
{
    template<typename ...S>
    struct no_memory : public std::variant<S...>
    {
        using std::variant<S...>::operator=;
    };

    template<typename S, typename ...T>
    void store(no_memory<T...>& m, S&& s)
    {
        m = std::forward<S>(s);
    }

    template<typename S, typename ...T>
    void store(no_memory<T...>& m)
    {
        store(m, S());
    }

    template<typename F, typename ...T>
    auto visit(F&& f, no_memory<T...>& m)
    {
        /*
         * gcc bug
         * You must not inherit from std::variant.
         * std::visit implementation uses std::variant_size<std::variant<T...>>.
         * There is no std::variant_size for any descended class.
         */
        return std::visit(std::forward<F>(f), static_cast<std::variant<T...>>(m));
    }
}
