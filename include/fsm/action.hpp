#pragma once
#include <optional>

namespace fsm::action
{
    struct none {};

    template<typename T>
    struct change
    {
        using state_type = T;

        change() : state(std::nullopt) {}
        change(state_type state) : state(state) {}

        std::optional<state_type> state;
    };
}
