#include "machine.hpp"
#include "transition.hpp"

namespace fsm
{
    template<typename M>
    struct Visitor
    {
        M& m;

        Visitor(M& m) : m(m) {};

        void operator()(action::none) {}

        template<typename S>
        void operator()(action::change<S>)
        {
            using state = typename action::change<S>::state_type;
            storage::store<state>(this->m.storage);
        }
    };

    template<typename M, typename ...E>
    void process(M& m, E... e)
    {
        std::visit(
            Visitor(m),
            storage::visit(
                [&](auto s)
                {
                    return transition(m, s, e...);
                },
                m.storage
            )
        );
    }
}
